import { Controller, Get } from '@nestjs/common';

import { TopicDto } from './dto/topic.dto';
import { TopicsService } from './topics.service';

@Controller('topics')
export class TopicsController {
  constructor(private topicsService: TopicsService) {}

  @Get()
  async findAll(): Promise<TopicDto[]> {
    return this.topicsService.topics;
  }
}
