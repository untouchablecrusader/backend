import { Injectable, OnModuleInit } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';

import staticTopics from './topics.json';
import { TopicDto } from './dto/topic.dto';

@Injectable()
export class TopicsService implements OnModuleInit {
  topics: TopicDto[] = [];

  onModuleInit() {
    this.topics = this.processStaticTopics(staticTopics);
  }

  processStaticTopics(topics) {
    return topics.map(item => ({
      ...item,
      id: uuidv4(),
      peopleInQueue: 0,
      peopleInTotal: 0,
    }));
  }

  findTopic(id: string): TopicDto {
    return this.topics.find(t => t.id === id);
  }

  adjustTopicQueue(topicId: string, amount: number): TopicDto {
    const topic = this.topics.find(topic => topic.id === topicId);

    topic.peopleInQueue = topic.peopleInQueue + amount;

    return topic;
  }

  adjustTopicTotal(topicId: string, amount: number): TopicDto {
    const topic = this.topics.find(topic => topic.id === topicId);

    topic.peopleInTotal = topic.peopleInTotal + amount;

    return topic;
  }
}
