import { Test, TestingModule } from '@nestjs/testing';

import { MatchesService } from './matches.service';

describe('MatchesService', () => {
  let service: MatchesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MatchesService],
    }).compile();

    service = module.get<MatchesService>(MatchesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should not be empty', () => {
    service.onModuleInit();

    expect(service.names.length).toBeGreaterThan(0);
  });

  it('should return random names', () => {
    service.onModuleInit();

    const namesA = new Array(10).fill(null).map(() => service.getRandomName());
    const namesB = new Array(10).fill(null).map(() => service.getRandomName());

    expect(JSON.stringify(namesA)).not.toBe(JSON.stringify(namesB));
  });
});
